package com.example.demovolley

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {
    lateinit var requestQueue:RequestQueue

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
            requestQueue = Volley.newRequestQueue(this)

        downloadButton.setOnClickListener {
            val url = urlField.text
          //launchDownloadRequest(url.toString())
          //launchJsonObjectRequest("https://jsonplaceholder.typicode.com/todos/1")
          launchJsonArrayRequest("https://jsonplaceholder.typicode.com/todos")
        }
    }

    fun launchJsonArrayRequest(url: String) {
        val jsonArrayRequest = JsonArrayRequest(url, Response.Listener { taskList ->
            val stringBuilder = StringBuilder()
                for (taskIndex in 0..(taskList.length() - 1)) {
                    val taskObject = taskList.getJSONObject(taskIndex)
                        stringBuilder.append("-" + taskObject.getString("title") + "\n")
                }
                outPutView.text =  stringBuilder.toString()
        }, Response.ErrorListener {  })
            requestQueue.add(jsonArrayRequest)
    }

    fun launchDownloadRequest(url:String) {
        val request = StringRequest(Request.Method.GET, url,
            Response.Listener { body:String ->
                outPutView.text = body
            }, Response.ErrorListener {
                outPutView.text = "error"
            })
        outPutView.text = "Chargement....."
        requestQueue.add(request)
    }

    fun launchJsonObjectRequest(url:String) {
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url,null,
            Response.Listener {taskObject ->
                outPutView.text = taskObject.getString("title")
            },
            Response.ErrorListener {
                outPutView.text = "error"
            })
        requestQueue.add(jsonObjectRequest)
    }
}